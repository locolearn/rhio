#include <iostream>
#include <thread>
#include <chrono>
#include "RhIO.hpp"

int main()
{
    {
        RhIO::reset();
        
        RhIO::Root.newChild("/dir");
        RhIO::Root.newFloat("/dir/value")
            ->persisted(true)
            ->defaultValue(0.0);
        
        RhIO::start();
        for (int i=0;i<10000;i++) {
            RhIO::Root.setFloat("/dir/value", (double)i);
        }
        RhIO::stop();
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
    {
        RhIO::reset();
        
        RhIO::Root.newChild("/dir");
        RhIO::Root.newFloat("/dir/value")
            ->persisted(true)
            ->defaultValue(0.0);
        
        RhIO::start();
        for (int i=0;i<10000;i++) {
            RhIO::Root.setFloat("/dir/value", (double)i);
        }
        RhIO::stop();
    }

    return 0;
}


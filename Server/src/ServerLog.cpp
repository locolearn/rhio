#include <fstream>
#include <stdexcept>
#include "rhio_server/ServerLog.hpp"

namespace RhIO {

ServerLog::ServerLog(
    size_t sizeFloat,
    size_t sizeOther) :
    _bufferBool(5000000UL),
    _bufferInt(5000000UL),
    _bufferFloat(5000000UL),
    _bufferStr(5000000UL),
    _mappingBool(),
    _mappingInt(),
    _mappingFloat(),
    _mappingStr(),
    _valuesBool(sizeOther),
    _valuesInt(sizeOther),
    _valuesFloat(sizeFloat),
    _valuesStr(sizeOther),
    _mutex()
{
}

void ServerLog::logBool(
    const std::string& name, 
    bool val, 
    int64_t timestamp)
{
    _bufferBool.appendFromWriter({name, timestamp, val});
}
void ServerLog::logInt(
    const std::string& name, 
    int64_t val, 
    int64_t timestamp)
{
    _bufferInt.appendFromWriter({name, timestamp, val});
}
void ServerLog::logFloat(
    const std::string& name, 
    double val, 
    int64_t timestamp)
{
    _bufferFloat.appendFromWriter({name, timestamp, val});
}
void ServerLog::logStr(
    const std::string& name, 
    const std::string& val, 
    int64_t timestamp)
{
    _bufferStr.appendFromWriter({name, timestamp, val});
}
        
void ServerLog::tick()
{
    std::lock_guard<std::mutex> lock(_mutex);
    
    //Swap double lock free 
    //buffer for all types
    _bufferBool.swapBufferFromReader();
    _bufferInt.swapBufferFromReader();
    _bufferFloat.swapBufferFromReader();
    _bufferStr.swapBufferFromReader();

    //Reference on value buffer to be logged
    const std::vector<LogNamedValBool>& bufBool = 
        _bufferBool.getBufferFromReader();
    size_t sizeBool = _bufferBool.getSizeFromReader();
    const std::vector<LogNamedValInt>& bufInt = 
        _bufferInt.getBufferFromReader();
    size_t sizeInt = _bufferInt.getSizeFromReader();
    const std::vector<LogNamedValFloat>& bufFloat = 
        _bufferFloat.getBufferFromReader();
    size_t sizeFloat = _bufferFloat.getSizeFromReader();
    const std::vector<LogNamedValStr>& bufStr = 
        _bufferStr.getBufferFromReader();
    size_t sizeStr = _bufferStr.getSizeFromReader();

    //Log values
    for (size_t i=0;i<sizeBool;i++) {
        if (_mappingBool.count(bufBool[i].name) == 0) {
            _mappingBool.insert(std::make_pair(
                bufBool[i].name, _mappingBool.size()));
        }
        size_t id = _mappingBool.at(bufBool[i].name);
        _valuesBool.append({
            id, bufBool[i].timestamp, bufBool[i].value});
    }
    for (size_t i=0;i<sizeInt;i++) {
        if (_mappingInt.count(bufInt[i].name) == 0) {
            _mappingInt.insert(std::make_pair(
                bufInt[i].name, _mappingInt.size()));
        }
        size_t id = _mappingInt.at(bufInt[i].name);
        _valuesInt.append({
            id, bufInt[i].timestamp, bufInt[i].value});
    }
    for (size_t i=0;i<sizeFloat;i++) {
        if (_mappingFloat.count(bufFloat[i].name) == 0) {
            _mappingFloat.insert(std::make_pair(
                bufFloat[i].name, _mappingFloat.size()));
        }
        size_t id = _mappingFloat.at(bufFloat[i].name);
        _valuesFloat.append({
            id, bufFloat[i].timestamp, bufFloat[i].value});
    }
    for (size_t i=0;i<sizeStr;i++) {
        if (_mappingStr.count(bufStr[i].name) == 0) {
            _mappingStr.insert(std::make_pair(
                bufStr[i].name, _mappingStr.size()));
        }
        size_t id = _mappingStr.at(bufStr[i].name);
        _valuesStr.append({
            id, bufStr[i].timestamp, bufStr[i].value});
    }
}

void ServerLog::writeLogsToFile(const std::string& filepath)
{
    std::lock_guard<std::mutex> lock(_mutex);
    std::ofstream file(filepath);
    if (file.is_open()) {
        //Write value name mapping sizes
        size_t sizeMapBool = _mappingBool.size();
        size_t sizeMapInt = _mappingInt.size();
        size_t sizeMapFloat = _mappingFloat.size();
        size_t sizeMapStr = _mappingStr.size();
        writeBinary(file, sizeMapBool);
        writeBinary(file, sizeMapInt);
        writeBinary(file, sizeMapFloat);
        writeBinary(file, sizeMapStr);
        //Write name mapping
        for (const auto& it : _mappingBool) {
            writeBinary(file, it.first);
            writeBinary(file, it.second);
        }
        for (const auto& it : _mappingInt) {
            writeBinary(file, it.first);
            writeBinary(file, it.second);
        }
        for (const auto& it : _mappingFloat) {
            writeBinary(file, it.first);
            writeBinary(file, it.second);
        }
        for (const auto& it : _mappingStr) {
            writeBinary(file, it.first);
            writeBinary(file, it.second);
        }
        //Write value data point sizes
        size_t sizeValBool = _valuesBool.size();
        size_t sizeValInt = _valuesInt.size();
        size_t sizeValFloat = _valuesFloat.size();
        size_t sizeValStr = _valuesStr.size();
        writeBinary(file, sizeValBool);
        writeBinary(file, sizeValInt);
        writeBinary(file, sizeValFloat);
        writeBinary(file, sizeValStr);
        //Write value data points
        for (size_t i=0;i<sizeValBool;i++) {
            writeBinary(file, _valuesBool[i].id);
            writeBinary(file, _valuesBool[i].timestamp);
            writeBinary(file, _valuesBool[i].value);
        }
        for (size_t i=0;i<sizeValInt;i++) {
            writeBinary(file, _valuesInt[i].id);
            writeBinary(file, _valuesInt[i].timestamp);
            writeBinary(file, _valuesInt[i].value);
        }
        for (size_t i=0;i<sizeValFloat;i++) {
            writeBinary(file, _valuesFloat[i].id);
            writeBinary(file, _valuesFloat[i].timestamp);
            writeBinary(file, _valuesFloat[i].value);
        }
        for (size_t i=0;i<sizeValStr;i++) {
            writeBinary(file, _valuesStr[i].id);
            writeBinary(file, _valuesStr[i].timestamp);
            writeBinary(file, _valuesStr[i].value);
        }
        file.close();
    } else {
        throw std::runtime_error(
            "RhIO::ServerLog::writeLogs: "
            "Unable to write file: " 
            + filepath);
    }
}

}


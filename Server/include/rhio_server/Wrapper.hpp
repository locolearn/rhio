#ifndef RHIO_WRAPPER_HPP
#define RHIO_WRAPPER_HPP

#include <vector>
#include <string>
#include <stdexcept>
#include "RhIO.hpp"
#include "rhio_common/Value.hpp"
#include "rhio_server/ValueNode.hpp"

namespace RhIO {

/**
 * Wrapper
 *
 * Object wrapper around an existing value 
 * used to speed up real time calls 
 * since no string name is needed.
 */
class WrapperBool
{
    public:
        
        /**
         * Default uninitialized
         */
        WrapperBool() :
            _node(nullptr),
            _ptrValue(nullptr)
        {
        }

        /**
         * Initialize with a RhIO node and the name of 
         * an existing boolean value stored within it
         */
        WrapperBool(
            IONode& node,
            const std::string& name) :
            _node(&node),
            _ptrValue(&(node.accessValueBool(name)))
        {
        }

        /**
         * Bind the wrapper to given RhIO node and given
         * name of existing boolean stored within it.
         * Only if the wrapper is still uninitialized.
         */
        void bind(
            IONode& node,
            const std::string& name)
        {
            if (_node != nullptr || _ptrValue != nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperBool::bind: "
                    "Wrapper already assigned: " + name);
            }
            _node = &node;
            _ptrValue = &(node.accessValueBool(name));
        }
        
        /**
         * Initialize the wrapper with given RhIO node and given
         * name of boolean value to be created.
         * Only if the wrapper is still uninitialized and the value does not exist.
         */
        std::unique_ptr<RhIO::ValueBuilderBool> create(
            IONode& node,
            const std::string& name)
        {
            if (_node != nullptr || _ptrValue != nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperBool::create: "
                    "Wrapper already assigned: " + name);
            }
            if (node.getValueType(name) != RhIO::NoValue) {
                throw std::logic_error(
                    "RhIO::WrapperBool::create: "
                    "Value already exists: " + name);
            }
            std::unique_ptr<RhIO::ValueBuilderBool> tmpBuilder = node.newBool(name);
            bind(node, name);
            return tmpBuilder;
        }

        /**
         * Return the boolean value
         */
        double get() const
        {
            if (_ptrValue == nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperBool::get: uninitialized");
            }
            return _ptrValue->value.load();
        }
        operator bool() const 
        { 
            return get(); 
        }

        /**
         * Set the boolean value with optional 
         * timestamp information
         */
        void set(
            bool val,
            int64_t timestamp = getRhIOTime())
        {
            if (_node == nullptr || _ptrValue == nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperBool::set: uninitialized");
            }
            _node->assignRTBool(*_ptrValue, val, timestamp);
        }
        void operator=(bool val) 
        { 
            set(val);
        }

        /**
         * Standard copy constructor and custom assignment
         * with stored value
         */
        WrapperBool(const WrapperBool& wrapper) :
            _node(wrapper._node),
            _ptrValue(wrapper._ptrValue)
        {
        }
        void operator=(const WrapperBool& wrapper) 
        {
            set(wrapper.get());
        }

    private:

        /**
         * Pointer towards a node and a boolean value 
         * element stored in the map container.
         */
        IONode* _node;
        ValueBool* _ptrValue;
};
class WrapperInt
{
    public:
        
        /**
         * Default uninitialized
         */
        WrapperInt() :
            _node(nullptr),
            _ptrValue(nullptr)
        {
        }

        /**
         * Initialize with a RhIO node and the name of 
         * an existing int value stored within it
         */
        WrapperInt(
            IONode& node,
            const std::string& name) :
            _node(&node),
            _ptrValue(&(node.accessValueInt(name)))
        {
        }

        /**
         * Bind the wrapper to given RhIO node and given
         * name of existing integer stored within it.
         * Only if the wrapper is still uninitialized.
         */
        void bind(
            IONode& node,
            const std::string& name)
        {
            if (_node != nullptr || _ptrValue != nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperInt::bind: "
                    "Wrapper already assigned: " + name);
            }
            _node = &node;
            _ptrValue = &(node.accessValueInt(name));
        }
        
        /**
         * Initialize the wrapper with given RhIO node and given
         * name of int value to be created.
         * Only if the wrapper is still uninitialized and the value does not exist.
         */
        std::unique_ptr<RhIO::ValueBuilderInt> create(
            IONode& node,
            const std::string& name)
        {
            if (_node != nullptr || _ptrValue != nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperInt::create: "
                    "Wrapper already assigned: " + name);
            }
            if (node.getValueType(name) != RhIO::NoValue) {
                throw std::logic_error(
                    "RhIO::WrapperInt::create: "
                    "Value already exists: " + name);
            }
            std::unique_ptr<RhIO::ValueBuilderInt> tmpBuilder = node.newInt(name);
            bind(node, name);
            return tmpBuilder;
        }

        /**
         * Return the integer value
         */
        double get() const
        {
            if (_ptrValue == nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperInt::get: uninitialized");
            }
            return _ptrValue->value.load();
        }
        operator int64_t() const 
        { 
            return get(); 
        }

        /**
         * Set the integer value with optional 
         * timestamp information
         */
        void set(
            int64_t val,
            int64_t timestamp = getRhIOTime())
        {
            if (_node == nullptr || _ptrValue == nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperInt::set: uninitialized");
            }
            _node->assignRTInt(*_ptrValue, val, timestamp);
        }
        void operator=(int64_t val) 
        { 
            set(val);
        }
        void operator=(int val) 
        { 
            set(val);
        }
        
        /**
         * Standard copy constructor and custom assignment
         * with stored value
         */
        WrapperInt(const WrapperInt& wrapper) :
            _node(wrapper._node),
            _ptrValue(wrapper._ptrValue)
        {
        }
        void operator=(const WrapperInt& wrapper) 
        {
            set(wrapper.get());
        }

    private:

        /**
         * Pointer towards a node and a integer value 
         * element stored in the map container.
         */
        IONode* _node;
        ValueInt* _ptrValue;
};
class WrapperFloat
{
    public:
        
        /**
         * Default uninitialized
         */
        WrapperFloat() :
            _node(nullptr),
            _ptrValue(nullptr)
        {
        }

        /**
         * Initialize with a RhIO node and the name of 
         * an existing float value stored within it
         */
        WrapperFloat(
            IONode& node,
            const std::string& name) :
            _node(&node),
            _ptrValue(&(node.accessValueFloat(name)))
        {
        }

        /**
         * Bind the wrapper to given RhIO node and given
         * name of existing float stored within it.
         * Only if the wrapper is still uninitialized.
         */
        void bind(
            IONode& node,
            const std::string& name)
        {
            if (_node != nullptr || _ptrValue != nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperFloat::bind: "
                    "Wrapper already assigned: " + name);
            }
            _node = &node;
            _ptrValue = &(node.accessValueFloat(name));
        }

        /**
         * Initialize the wrapper with given RhIO node and given
         * name of float value to be created.
         * Only if the wrapper is still uninitialized and the value does not exist.
         */
        std::unique_ptr<RhIO::ValueBuilderFloat> create(
            IONode& node,
            const std::string& name)
        {
            if (_node != nullptr || _ptrValue != nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperFloat::create: "
                    "Wrapper already assigned: " + name);
            }
            if (node.getValueType(name) != RhIO::NoValue) {
                throw std::logic_error(
                    "RhIO::WrapperFloat::create: "
                    "Value already exists: " + name);
            }
            std::unique_ptr<RhIO::ValueBuilderFloat> tmpBuilder = node.newFloat(name);
            bind(node, name);
            return tmpBuilder;
        }

        /**
         * Return the float value
         */
        double get() const
        {
            if (_ptrValue == nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperFloat::get: uninitialized");
            }
            return _ptrValue->value.load();
        }
        operator double() const 
        { 
            return get(); 
        }

        /**
         * Set the float value with optional 
         * timestamp information
         */
        void set(
            double val,
            int64_t timestamp = getRhIOTime())
        {
            if (_node == nullptr || _ptrValue == nullptr) {
                throw std::logic_error(
                    "RhIO::WrapperFloat::set: uninitialized");
            }
            _node->assignRTFloat(*_ptrValue, val, timestamp);
        }
        void operator=(double val) 
        { 
            set(val);
        }
        void operator=(float val) 
        { 
            set(val);
        }

        /**
         * Standard copy constructor and custom assignment
         * with stored value
         */
        WrapperFloat(const WrapperFloat& wrapper) :
            _node(wrapper._node),
            _ptrValue(wrapper._ptrValue)
        {
        }
        void operator=(const WrapperFloat& wrapper) 
        {
            set(wrapper.get());
        }

    private:

        /**
         * Pointer towards a node and a float value 
         * element stored in the map container.
         */
        IONode* _node;
        ValueFloat* _ptrValue;
};

class WrapperArray
{
    public:
        
        /**
         * Default uninitialized
         */
        WrapperArray() :
            _container()
        {
        }
        
	/**
         * Bind the wrapper to given RhIO node and given
         * name of existing float stored within it.
         * Only if the wrapper is still uninitialized.
         */
        void bind(
            IONode& node,
            const std::string& name,
            size_t size)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperArray::bind: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<size;i++) {
                _container.push_back(RhIO::WrapperFloat());
                _container.back().bind(node, name + std::string("_") + std::to_string(i));
            }
        }
        
        /**
         * Initialize the wrapper with given RhIO node and given
         * name of float value to be created.
         * Only if the wrapper is still uninitialized and the value does not exist.
         */
        void create(
            IONode& node,
            const std::string& name,
            size_t size)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperArray::create: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<size;i++) {
                _container.push_back(RhIO::WrapperFloat());
                _container.back().create(node, name + std::string("_") + std::to_string(i));
            }
        }

        /**
         * Initialize the wrapper by adding and creating 
         * a new float value with given name.
         */
        void append(
            IONode& node,
            const std::string& name)
        {
            _container.push_back(RhIO::WrapperFloat());
            _container.back().create(node, name);
        }
        
        /**
         * Return the indexed float value
         */
        double get(size_t index) const
        {
            return _container.at(index).get();
        }

        /**
         * Set the indexed float value with optional 
         * timestamp information
         */
        void set(
            size_t index,
            double val,
            int64_t timestamp = getRhIOTime())
        {
            _container.at(index).set(val, timestamp);
        }

        /**
         * Overloaded operators for get and set with array interface
         */
        const RhIO::WrapperFloat& operator[](size_t index) const
        {
            return _container.at(index);
        }
        RhIO::WrapperFloat& operator[](size_t index)
        {
            return _container.at(index);
        }
        template <typename T>
        void operator=(const T& array) 
        {
            for (size_t i=0;i<_container.size();i++) {
                set(i, array[i]);
            }
        }
        
        /**
         * Overloaded conversion cast operator 
         * if needed by user. Deleted by default.
         */
        template <typename T>
        operator T() const = delete;

    protected:

        /**
         * Array of internal float wrapper
         */
        std::vector<RhIO::WrapperFloat> _container;
};

class WrapperVect2d : public WrapperArray
{
    public:
        
	/**
         * Override wrapper binding for 2d vector suffixes
         */
        void bind(
            IONode& node,
            const std::string& name)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperVect2d::bind: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<2;i++) {
                _container.push_back(RhIO::WrapperFloat());
            }
            _container[0].bind(node, name + std::string("_x"));
            _container[1].bind(node, name + std::string("_y"));
        }
        
        /**
         * Override wrapper creation for 2d vector suffixes
         */
        void create(
            IONode& node,
            const std::string& name)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperVect2d::create: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<2;i++) {
                _container.push_back(RhIO::WrapperFloat());
            }
            _container[0].create(node, name + std::string("_x"));
            _container[1].create(node, name + std::string("_y"));
        }
        
        /**
         * Overloaded operators for array interface
         */
        template <typename T>
        void operator=(const T& array) 
        {
            for (size_t i=0;i<2;i++) {
                set(i, array[i]);
            }
        }
        
        /**
         * Overloaded conversion cast operator 
         * if needed by user. Deleted by default.
         */
        template <typename T>
        operator T() const = delete;

        /**
         * Shorthand element access
         */
        const RhIO::WrapperFloat& x() const
        {
            return _container.at(0);
        }
        RhIO::WrapperFloat& x()
        {
            return _container.at(0);
        }
        const RhIO::WrapperFloat& y() const
        {
            return _container.at(1);
        }
        RhIO::WrapperFloat& y()
        {
            return _container.at(1);
        }
};
class WrapperVect3d : public WrapperArray
{
    public:
        
	/**
         * Override wrapper binding for 3d vector suffixes
         */
        void bind(
            IONode& node,
            const std::string& name)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperVect3d::bind: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<3;i++) {
                _container.push_back(RhIO::WrapperFloat());
            }
            _container[0].bind(node, name + std::string("_x"));
            _container[1].bind(node, name + std::string("_y"));
            _container[2].bind(node, name + std::string("_z"));
        }
        
        /**
         * Override wrapper creation for 3d vector suffixes
         */
        void create(
            IONode& node,
            const std::string& name)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperVect3d::create: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<3;i++) {
                _container.push_back(RhIO::WrapperFloat());
            }
            _container[0].create(node, name + std::string("_x"));
            _container[1].create(node, name + std::string("_y"));
            _container[2].create(node, name + std::string("_z"));
        }

        /**
         * Overloaded operators for array interface
         */
        template <typename T>
        void operator=(const T& array) 
        {
            for (size_t i=0;i<3;i++) {
                set(i, array[i]);
            }
        }

        /**
         * Overloaded conversion cast operator 
         * if needed by user. Deleted by default.
         */
        template <typename T>
        operator T() const = delete;
        
        /**
         * Shorthand element access
         */
        const RhIO::WrapperFloat& x() const
        {
            return _container.at(0);
        }
        RhIO::WrapperFloat& x()
        {
            return _container.at(0);
        }
        const RhIO::WrapperFloat& y() const
        {
            return _container.at(1);
        }
        RhIO::WrapperFloat& y()
        {
            return _container.at(1);
        }
        const RhIO::WrapperFloat& z() const
        {
            return _container.at(2);
        }
        RhIO::WrapperFloat& z()
        {
            return _container.at(2);
        }
};
class WrapperVect4d : public WrapperArray
{
    public:
        
	/**
         * Override wrapper binding for 4d vector suffixes
         */
        void bind(
            IONode& node,
            const std::string& name)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperVect4d::bind: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<4;i++) {
                _container.push_back(RhIO::WrapperFloat());
            }
            _container[0].bind(node, name + std::string("_x"));
            _container[1].bind(node, name + std::string("_y"));
            _container[2].bind(node, name + std::string("_z"));
            _container[3].bind(node, name + std::string("_w"));
        }
        
        /**
         * Override wrapper creation for 2d vector suffixes
         */
        void create(
            IONode& node,
            const std::string& name)
        {
            if (_container.size() != 0) {
                throw std::logic_error(
                    "RhIO::WrapperVect4d::create: "
                    "Wrapper already initialized: " + name);
            }
            for (size_t i=0;i<4;i++) {
                _container.push_back(RhIO::WrapperFloat());
            }
            _container[0].create(node, name + std::string("_x"));
            _container[1].create(node, name + std::string("_y"));
            _container[2].create(node, name + std::string("_z"));
            _container[3].create(node, name + std::string("_w"));
        }
        
        /**
         * Overloaded operators for array interface
         */
        template <typename T>
        void operator=(const T& array) 
        {
            for (size_t i=0;i<4;i++) {
                set(i, array[i]);
            }
        }
        
        /**
         * Overloaded conversion cast operator 
         * if needed by user. Deleted by default.
         */
        template <typename T>
        operator T() const = delete;
        
        /**
         * Shorthand element access
         */
        const RhIO::WrapperFloat& x() const
        {
            return _container.at(0);
        }
        RhIO::WrapperFloat& x()
        {
            return _container.at(0);
        }
        const RhIO::WrapperFloat& y() const
        {
            return _container.at(1);
        }
        RhIO::WrapperFloat& y()
        {
            return _container.at(1);
        }
        const RhIO::WrapperFloat& z() const
        {
            return _container.at(2);
        }
        RhIO::WrapperFloat& z()
        {
            return _container.at(2);
        }
        const RhIO::WrapperFloat& w() const
        {
            return _container.at(3);
        }
        RhIO::WrapperFloat& w()
        {
            return _container.at(3);
        }
};

}

#endif


#ifndef RHIO_CIRCULARBUFFER_HPP
#define RHIO_CIRCULARBUFFER_HPP

#include <vector>

namespace RhIO {

/**
 * CircularBuffer
 *
 * Template type circular buffer
 * with pre-allocated memory.
 */
template <typename T>
class CircularBuffer
{
    public:

        /**
         * Initialization with maximum 
         * pre-allocated buffer size
         */
        CircularBuffer(size_t maxSize) :
            _buffer(),
            _head(0),
            _size(0)
        {
            clear();
            _buffer.resize(maxSize);
        }
        
        /**
         * @return the number of written 
         * element in the buffer
         */
        size_t size() const
        {
            return _size;
        }

        /**
         * Reset internal buffer to empty
         */
        void clear()
        {
            _head = 0;
            _size = 0;
        }
        
        /**
         * Append given data element into 
         * the circular buffer
         */
        void append(const T& value)
        {
            _buffer[_head] = value;
            if (_size < _buffer.size()) {
                _size++;
            }
            _head++;
            if (_head >= _buffer.size()) {
                _head = 0;
            }
        }
        
        /**
         * @return the stored value indexed backward 
         * in time by its index (from 0 to size()-1).
         */
        const T& get(size_t index) const
        {
            if (_size == 0) {
                //Buffer is not yet initialized
                throw std::logic_error(
                    "RhIO::CircularBuffer::get: Invalid access");
            }
            if (index >= _size) {
                //Index is outside available data
                throw std::logic_error(
                    "RhIO::CircularBuffer::get: Invalid access");
            }
            if (index+1 <= _head) {
                return _buffer[_head-1-index];
            } else {
                return _buffer[_buffer.size()-(index+1-_head)];
            }
        }

        /**
         * Overload array operator for access to data
         * in oldest first order
         */
        const T& operator[](size_t index) const
        {
            return get(_size-index-1);
        }

    private:
        
        /**
         * Allocated data 
         * circular buffer
         */
        std::vector<T> _buffer;
        
        /**
         * Index to next buffer element 
         * to be written
         */
        size_t _head;

        /**
         * Current buffer used 
         * elements size
         */
        size_t _size;
};

}

#endif

